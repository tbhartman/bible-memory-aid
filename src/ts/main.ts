namespace bma {
  export function Init(){
    return new BMA(
      document.getElementById("control") as HTMLDivElement,
      document.getElementById("before") as HTMLDivElement,
      document.getElementById("after") as HTMLDivElement,
    );
  }

  interface bibleApiResponse {
    status:number
    data:any
  }

  interface bibleId {
    id:string
    abbreviation:string
  }

  class BibleApi {
    private scheme:string = "https://"
    private host:string = "api.scripture.api.bible"

    constructor(private token:string) {

    }

    private call(path:string, parameters:[string,string][]):Promise<bibleApiResponse> {
      var xhttp = new XMLHttpRequest();
      var url = new URL(this.scheme + this.host + "/v1" + path);
      parameters.forEach(element => {
        url.searchParams.append(element[0], element[1]);
      });
      console.log(url);
			xhttp.open("GET", url.href, true);
      xhttp.setRequestHeader("api-key", this.token);
      xhttp.setRequestHeader("accept", "application/json");
			return new Promise<bibleApiResponse>((resolve, reject) => {
				xhttp.onreadystatechange = function () {
          let data:any;
          console.error(xhttp.responseType);
          console.error(xhttp.response.length);
					try {
						data = JSON.parse(xhttp.response);
					} catch (error) {
            console.log(xhttp.response);
						console.error("Failed to parse JSON response");
						reject(error);
					}
          resolve({
            status: xhttp.status,
            data: data
          });
				}
				xhttp.send();
			});
    }

    getTranslations():Promise<bibleId[]> {
      return this.call("/bibles", [["language","eng"],["include-full-details","false"]]).then((r)=>{
        var bibles:bibleId[] = [];
        r.data.forEach((element:any) => {
          bibles.push({
            id: element.id,
            abbreviation: element.abbreviationLocal
          });
        });
        return bibles;
      })
    }
  }

  interface bmaControls {
    versions?:HTMLSelectElement
    books?:HTMLSelectElement
    start?:HTMLSelectElement
    end?:HTMLSelectElement
    type:HTMLSelectElement
  }

  interface Summarizer {
    identifier:string
    name:string
    Render(input:string):string
  }

  interface verse {
    number:number
    text:string
  }
  interface translateError {
    error:string
  }

  function translate(s:string):verse[]|translateError {
    var i:number = 1;
    if (!s.startsWith("1 ")) {
      return {
        error: "Input must start with `1 `"
      }
    }
    s = s.substr(2);
    var ret:verse[] = [];
    while (true) {
      var re:RegExp = new RegExp("\\s+" + (i+1).toString() + "\\s+");
      var result = re.exec(s);
      if (result === null) {
        ret.push({
          number: i,
          text: s
        });
        break;
      }
      ret.push({
        number: i,
        text: s.substr(0, result.index)
      });
      i++;
      s = s.substr(result.index + result[0].length);
    }
    return ret;
  }

  class ByWord implements Summarizer {
    identifier = "byword"
    name = "by word"
    Render(input:string):string {

      var verses = translate(input);
      if (!("error" in verses)) {
        var output = '<div>';
        verses.forEach((e,i) => {
            output += ' <span style="color:lightgray; font-size: 10pt">' + e.number.toString() + '</span>';
            var text = e.text;
            var r = /\w+/.exec(text);
            if (r === null) {
              return;
            }
            output += r[0];
        });
        output += "</div>";
        return output;
      } else {
        return verses.error;
      }
    }
  }
  class ByLetter implements Summarizer {
    identifier = "byletter"
    name = "by letter"
    Render(input:string):string {
      var verses = translate(input);
      if (!("error" in verses)) {
        var output = '<div style="overflow-wrap: anywhere;">';
        verses.forEach((e,i) => {
            output += '<span style="color:lightgray; font-size: 10pt">' + e.number.toString() + '</span>';
            var text = e.text;
            while (true) {
              var r = /\w+/.exec(text);
              if (r === null) {
                break;
              }
              output += r[0].substr(0,1);
              text = text.substr(r.index + r[0].length);
            }
        });
        output += "</div>";
        return output;
      } else {
        return verses.error;
      }
    }
  }
  class Regular implements Summarizer {
    identifier = "regular"
    name = "regular"
    Render(input:string):string {
      var verses = translate(input);
      if (!("error" in verses)) {
        var output = '<div>';
        verses.forEach((e,i) => {
            output += ' <span style="color:lightgray; font-size: 10pt; position: relative; bottom: 3px;">' + e.number.toString() + '</span>';
            output += e.text;
        });
        output += "</div>";
        return output;
      } else {
        return verses.error;
      }
    }
  }

  class BMA {
    statusP:HTMLParagraphElement
    conn:BibleApi
    controls:bmaControls

    testIn:HTMLTextAreaElement
    testOut:HTMLDivElement

    summarizeTypes:Summarizer[] = [new ByWord(), new Regular() , new ByLetter()]

    constructor(private control:HTMLDivElement, private before:HTMLDivElement, private after:HTMLDivElement) {
      this.Init();
    }

    private status(s:string) {
      this.statusP.innerText = s;
    }

    private Init() {
      this.statusP = document.createElement("p");
      this.status("initializing");
      this.initSimple();
      // this.initView();
      // this.initConnection();
    }
    private initSimple() {
      this.testIn = document.createElement("textarea");
      this.testOut = document.createElement("div");
      this.before.appendChild(this.testIn);
      this.after.appendChild(this.testOut);
      this.testIn.value = bma.sample;

      this.controls = {
        type: document.createElement("select")
      };
      var label = this.before.appendChild(document.createElement("label"));
      label.setAttribute("for", "summarize-type");
      this.before.appendChild(this.controls.type);
      this.controls.type.id = "summarize-type";
      
      this.summarizeTypes.forEach(s => {
        var i = document.createElement("option");
        i.value = s.identifier;
        i.innerText = s.name;
        this.controls.type.appendChild(i);
      });

      this.controls.type.onchange = this.update.bind(this);
      this.testIn.onkeyup = this.update.bind(this);
      this.update();
    }
    private update() {
      var input = this.testIn.value;
      var summarizer:Summarizer|undefined;
      this.summarizeTypes.forEach(element => {
        if (this.controls.type.options[this.controls.type.selectedIndex].value == element.identifier) {
          summarizer = element;
        }
      });
      if (summarizer === undefined) {
        alert("this is bad");
        return
      }
      var output = summarizer.Render(input);
      this.testOut.innerHTML = output;
    }
    private initConnection() {
      // this.conn = new BibleApi(ScriptureApiToken);
      // this.conn.getTranslations().then((bb)=>{
      //   bb.forEach(b => {
      //     var opt = document.createElement("option");
      //     this.controls.versions.appendChild(opt);
      //     opt.value = b.id;
      //     opt.innerText = b.abbreviation;
      //   });
      // });
    }
    private initView() {
      var form = document.createElement("form");
      var version = document.createElement("select");
      var book = document.createElement("select");
      var startChapter = document.createElement("select");
      var endChapter = document.createElement("select");

      // this.controls = {
      //   books:book,
      //   end:endChapter,
      //   start:startChapter,
      //   versions:version,

      // };

      this.control.appendChild(this.statusP);
      this.control.appendChild(form);

      var versionLabel = form.appendChild(document.createElement("label"));
      form.appendChild(version);
      var bookLabel = form.appendChild(document.createElement("label"));
      form.appendChild(book);
      var startLabel = form.appendChild(document.createElement("label"));
      form.appendChild(startChapter);
      var endLabel = form.appendChild(document.createElement("label"));
      form.appendChild(endChapter);

      version.id = "version";
      book.id = "book";
      startChapter.id = "start";
      endChapter.id = "end";

      versionLabel.setAttribute("for", "version");
      bookLabel.setAttribute("for", "book");
      startLabel.setAttribute("for", "start");
      endLabel.setAttribute("for", "end");

      versionLabel.innerText = "Translation";
      bookLabel.innerText = "Book";
      startLabel.innerText = "Start Chapter";
      endLabel.innerText = "End Chapter";

    }
  }
}
