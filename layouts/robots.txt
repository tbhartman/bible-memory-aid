User-agent: *
Disallow: /cv
Allow: /cv/colophon

Sitemap: {{ .Site.BaseURL }}/sitemap.xml
